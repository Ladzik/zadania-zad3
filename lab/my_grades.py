# -*- encoding: utf-8 -*-

import getpass
import requests
from bs4 import BeautifulSoup

url = {
    'login': 'https://canvas.instructure.com/login',
    'grades': 'https://canvas.instructure.com/courses/838324/grades'
}

username = raw_input('Użytkownik: ')
password = getpass.getpass('Hasło: ')

payload = {
    'pseudonym_session[unique_id]': username,
    'pseudonym_session[password]': password
}

# Logowanie
session = requests.session()
session.post('https://canvas.instructure.com/login', data=payload)
# Pobranie strony z ocenami
response = session.get('https://canvas.instructure.com/courses/838324/grades')
# Odczyt strony
html = response.text
# Parsowanie strony
parsed = BeautifulSoup(html, 'html5lib')
# Scraping
forum = []
lab = []
pd = []
egz = []

oceny = parsed.find(id="grades_summary")
kategorie_ocen = oceny.find_all("div", "context", recursive=True)
nazwy = oceny.find_all("th", "title", recursive=True)
wartosci = oceny.find_all("span", "score", recursive=True)

for i in range(0, 19):
    if kategorie_ocen[i].text == u"Aktywność":
        forum.append([nazwy[i].text, nazwy[i].a["href"], wartosci[i].text.strip()])
    elif kategorie_ocen[i].text  == "Egzamin":
        egz.append([nazwy[i].text, nazwy[i].a["href"], wartosci[i].text.strip()])
    elif kategorie_ocen[i].text == "Laboratoria":
        lab.append([nazwy[i].text, nazwy[i].a["href"], wartosci[i].text.strip()])
    else:
        pd.append([nazwy[i].text, nazwy[i].a["href"], wartosci[i].text.strip()])

# Sortowanie



forum.sort(key=lambda x: x[2], reverse=True)
pd.sort(key=lambda x: x[2], reverse=True)
egz.sort(key=lambda x: x[2], reverse=True)
lab.sort(key=lambda x: x[2], reverse=True)

# Wyświetlenie posortowanych ocen w kategoriach



print("Aktywnosc: ")
for a in forum:
    print("%s, %s, %s" % (a[0],a[1],a[2]))

print("Prace domowe: ")
for a in pd:
    print("%s, %s, %s" % (a[0],a[1],a[2]))

print("Laboratoria: ")
for a in lab:
    print("%s, %s, %s" % (a[0],a[1],a[2]))

print("Egzamin: ")
for a in egz:
    print("%s, %s, %s" % (a[0],a[1],a[2]))